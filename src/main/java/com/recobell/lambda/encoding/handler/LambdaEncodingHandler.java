package com.recobell.lambda.converter;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.zip.GZIPInputStream;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

public class RecResultEncodingHandler
    implements RequestHandler<S3Event, String>
{
    @Override
    public String handleRequest(S3Event s3event, Context context) {

        S3EventNotificationRecord record = s3event.getRecords().get(0);
        String srcBucket = record.getS3().getBucket().getName();
        String srcKey = record.getS3().getObject().getKey();
        BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIWEIFVATC36WDR3Q", "4sHqgpAmR4vrJbwSuYYM4lbPQzi9zV47fuBVoY/K");
        AmazonS3Client s3 = new AmazonS3Client(awsCreds);

        //TODO:CSV+GZIP -> TSV+raw
        if (srcKey.contains("csv") && srcKey.contains("gz")) {
            try {

                String fileName = parseFileName(srcKey.split("/"));
                String convertedFileName = modifyFileExtension(fileName);
                String filePath = setS3ObjectPath(srcKey, convertedFileName);
                File convertedFile = fileConverter(srcBucket, srcKey, s3);
                export(s3, srcBucket, filePath, convertedFile);
            } catch (Exception e) {
                // TODO Auto-generated catch block
            }
        }
        
        return "Ok";
    }

    private File fileConverter(String srcBucket, String srcKey, AmazonS3Client s3)
        throws AmazonServiceException, AmazonClientException, IOException
    {
        try (
            InputStreamReader isr = getEncodedS3ObjectReader(s3, srcBucket, srcKey, "UTF8");
            BufferedReader br = new BufferedReader(isr);)
        {
            return convert(br, "UTF8", "EUC_KR");
        }

    }

    private InputStreamReader getEncodedS3ObjectReader(AmazonS3Client s3, String srcBucket, String srcKey, String encodingType)
        throws IOException
    {
        S3ObjectInputStream is = s3.getObject(srcBucket, srcKey).getObjectContent();
        GZIPInputStream gis = new GZIPInputStream(is);
        return new InputStreamReader(gis, encodingType);
    }

    private File convert(BufferedReader br, String encodedType, String encodingType)
        throws IOException
    {
        File tempFile = File.createTempFile("convertdump", ".tmp");
        FileOutputStream fos = new FileOutputStream(tempFile);
        System.out.println("convert lines start");
        CSVParser csvParser = new CSVParser(br, CSVFormat.DEFAULT);
        String tsvTemp = "";
        for (CSVRecord record : csvParser) {
            tsvTemp = "";
            for (int i = 0; i < record.size(); i++) {
                if (i == 0)
                    tsvTemp += record.get(0);
                else
                    tsvTemp += "\t" + record.get(i);
            }
            tsvTemp += "\n";
            writeFile(fos, stringEncoder(tsvTemp, encodedType, encodingType));
        }

        System.out.println("lines converted");
        csvParser.close();
        fos.close();
        return tempFile;
    }

    private void writeFile(FileOutputStream fos, String line)
        throws IOException
    {
        fos.write(line.getBytes());
    }

    private String stringEncoder(String sourceString, String encodedType, String encodingType)
        throws IOException
    {

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        byteStream.write(sourceString.getBytes(encodedType));
        return byteStream.toString(encodingType);
    }

    private String setS3ObjectPath(String srcKey, String fileName) {

        String temp[] = srcKey.split("/");
        String filePath = "";
        for (int i = 0; i < temp.length - 1; i++) {
            if (i == 0)
                filePath += temp[0];
            else
                filePath += "/" + temp[i];
        }
        return filePath + "/" + fileName;
    }

    private String parseFileName(String[] stringArray)
        throws Exception
    {
        return getLastIndexString(stringArray);
    }

    private String getLastIndexString(String[] stringArray)
        throws Exception
    {
        if (stringArray.length == 0) throw new Exception("zero size array!");
        return stringArray[stringArray.length - 1];
    }

    private String modifyFileExtension(String filePath) {
        String baseFileName = filePath.split("\\.")[0];
        return String.join(".", baseFileName, "tsv");
    }

    private void export(AmazonS3Client s3, String srcBucket, String filePath, File tempFile) {

        try {
            s3.putObject(srcBucket, filePath, tempFile);
        } catch (AmazonServiceException e) {
            System.out.println("put object exception");
        }
    }

}
